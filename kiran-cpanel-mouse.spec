Name:		kiran-cpanel-mouse
Version:	2.2.1
Release:	4
Summary:	This program provides mouse and touchpad settings.

Group:		System Environment/Base
License:	MulanPSL-2.0
URL:		http://gitlab.kylinos.com.cn/os/kiran-cpanel-mouse
Source0:	%{name}-%{version}.tar.gz

BuildRequires:	qt5-qtsvg-devel
BuildRequires: 	cmake
BuildRequires:	gettext
BuildRequires:	gcc-c++
BuildRequires:	qt5-qtbase-devel
BuildRequires:	qt5-linguist
BuildRequires:	kiran-widgets-qt5-devel
BuildRequires:	kiran-cc-daemon-devel >= 2.0.4
BuildRequires:	libstdc++-devel
BuildRequires:	kiran-log-qt5-devel
BuildRequires:  kiran-control-panel-devel
BuildRequires:  kiran-qdbusxml2cpp

Requires:   	gcc
Requires:	qt5-qtbase
Requires:	qt5-qtsvg
Requires:	kiran-widgets-qt5
Requires:	kiran-session-daemon >= 2.0.4
Requires:	kiran-log-qt5
Requires:       kiran-cpanel-launcher
Requires:       gtk-update-icon-cache

%description
This program provides mouse and touchpad settings.

%prep
%autosetup -p1

%build
%{__mkdir} -p %{buildroot}
%cmake 
make %{?_smp_mflags}

%install
%make_install

%post
gtk-update-icon-cache -f /usr/share/icons/hicolor/

%posttrans
%define link_source %{_datadir}/kiran-control-panel/plugins/desktop/%{name}.desktop
%define link_target %{_datadir}/applications/%{name}.desktop
if [ ! -e %{link_target} ];then
ln -sf %{link_source} %{link_target}
echo "link %{link_source} -> %{link_target}"
fi

%files
%doc
%{_datadir}/kiran-control-panel/plugins/libs/libkiran-cpanel-mouse.so*
%{_datadir}/%{name}/icons/*
%{_datadir}/icons/hicolor/*
%{_datadir}/%{name}/translations/*
%{_datadir}/kiran-control-panel/plugins/desktop/*
%{_datadir}/applications/kiran-cpanel-mouse.desktop

%clean
rm -rf %{buildroot}

%changelog
* Wed Aug 10 2022 luoqing <luoqing@kylinsec.com.cn> - 2.2.1-4
- KYOS-F: Modify license and add yaml file.

* Tue Mar 01 2022 longcheng <longcheng@kylinos.com.cn> - 2.2.1-3
- KYOS-B: Add Requires gtk-update-icon-cache

* Sat Feb 26 2022 longcheng <longcheng@kylinos.com.cn> - 2.2.1-2
- KYOS-R: rebuild for KY3.4-3-LS-dev

* Sat Feb 26 2022 longcheng <longcheng@kylinos.com.cn> - 2.2.1-1
- KYOS-B: fix build error in KY3.4-3-LS

* Thu Feb 24 2022 chendingjian <chendingjian@kylinos.com.cn> - 2.2.0-3
- rebuild for KY3.4-MATE-modules-dev

* Wed Dec 29 2021 kpkg <kpkg@kylinos.com.cn> - 2.2.0-2.kb1
- rebuild for KY3.4-MATE-modules-dev

* Wed Dec 29 2021 caoyuanji<caoyuanji@kylinos.com.cn> - 2.2.0-2
- Upgrade version number for easy upgrade

* Mon Dec 20 2021 caoyuanji <caoyuanji@kylinos.com.cn> - 2.2.0-1.kb3
- rebuild for KY3.4-4-KiranUI-2.2

* Mon Dec 20 2021 caoyuanji <caoyuanji@kylinos.com.cn> - 2.2.0-1.kb2
- rebuild for KY3.4-4-KiranUI-2.2

* Fri Nov 12 2021 yuanxing <yuanxing@kylinos.com.cn> - 2.2.0.kb1
- KYOS-F: refactoring code and automatic generate dbus file 
- KYOS-F: add Mulan PSL v2 license in code
- KYOS-F: add connect signal from dbus function and add varied sizes of icon 

* Wed Jul 21 2021 yuanxing <yuanxing@kylinos.com.cn> - 2.1.0.kb4
- KYOS-F: add scroll area in pages
- KYOS-F: add symbolic link of desktop file for show in mate-control-center

* Thu Jul 1 2021 yuanxing <yuanxing@kylinos.com.cn> - 2.1.0.kb3
- KYOS-F: add Keyword in chinese and english (#40693)
- KYOS-F: don't return when interface load translation failed (#40693)

* Thu Jun 17 2021 yuanxing <yuanxing@kylinos.com.cn> - 2.1.0.kb2
- KYOS-F: change the display mode of the slider bar value change 
- KYOS-F: change the name in desktop file

* Thu Jun 10 2021 yuanxing <yuanxing@kylinos.com.cn> - 2.1.0.kb1
- Initial build




